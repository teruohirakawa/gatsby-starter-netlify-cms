import React from 'react'
import Layout from '../../components/Layout'

export default () => (
  <Layout>
    <section className="section">
      <div className="container">
        <div className="content">
          <h1>お問い合わせ頂きありがとうございます</h1>
          <p>２～３営業日中に担当者よりご連絡させていただきます。</p>
        </div>
      </div>
    </section>
  </Layout>
)
