---
templateKey: index-page
title: 神戸の不動産/駐車場をご紹介
image: /img/kobe_landscape.jpg
heading: 神戸の不動産をご紹介・仲介
subheading: ☎　078-242-3451 　兵庫県神戸市中央区神若通３丁目３-１０
mainpitch:
  title: 物件・駐車場の仲介いたします。
  description: |
    弊社では、住居・駐車場の賃貸のご紹介。
description: 住居/駐車場の賃貸、売買の仲介を行っております。
intro:
  blurbs:
    - image: /img/home.png
      text: |
        賃貸用物件のご紹介、売買のご検討はぜひ弊社にご相談ください。お取り扱い、管理物件をご紹介いたします。
    - image: /img/parking.png
      text: 灘区水道筋商店街・阪急王子公園駅徒歩１分の駐車場がございます。
    - image: /img/dscf2308.jpg
      text: |
        神戸大丸1Fカフェの壁画素敵です。
    - image: /img/ポポとピアノ.jpg
      text: オカメインコさんガリバー気分。
  heading: あああ
  description: |+
    弊社では、住居・駐車場の賃貸のご紹介いたしております。

main:
  heading: 神戸を中心とした関西の不動産を取り扱っております。これからマイホームをこれから単身赴任で賃貸物件を。。。お客様のニーズにお応えできるよう、また安心して契約から引っ越しまでサポートいたしております。
  description: "\n"
  image1:
    alt: 神戸に住んで中央区にある大丸1Fのカフェで朝食はいかがですか？
    image: /img/dscf2308.jpg
  image2:
    alt: 虹が出ていました。撮影地は神戸市中央区の13Fダイニングバーです。
    image: /img/神戸虹.jpg
  image3:
    alt: 夕方の垂水区舞子の海です。パソナグループが移転する淡路島も写ってます。
    image: /img/maiko2.jpg
---
