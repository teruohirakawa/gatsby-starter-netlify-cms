---
heading: 神戸＊大阪を中心に関西一円の物件のご紹介を致しております。
path: /products
templateKey: product-page
title: 事業紹介
image: /img/kobe_overview.jpg
description: お客様のニーズにお応えできるように物件の改装、引越し、残材の処分等でストレスフリーで新居に引っ越していただくお手伝いもいたしております。
intro:
  blurbs:
    - image: /img/ロゴのみ.png
      text: |+
        弊社のロゴマークです。住居の中に普通にあるもの。
        植物（チューリップ）、スプーンとフォーク、絵画、いろんな要素を入れたロゴになっています。
        私たちの日常はいろんなものに囲まれて豊かさにあふれている。
        そんなお家に出会っていただくお手伝いができる会社を目指しています。

    - image: /img/ijinkabuke.jpg
      text: |
        神戸北野異人館で撮影したブーケです。
        綺麗なバラが見つかったのでフォーカルポイントにしました。
        バラが優しい色なので全体に軟らかい仕上がりになりました。
        お気に入りのブーケです。
    - image: /img/img_1013.jpg
      text: |
        沖縄糸満市。
        那覇空港から車で15分程度のビーチです。
        夕焼けが帯状の雲を映しているのが珍しく、とってもきれいでした。
        今はコロナで見に行けませんが。
    - image: /img/maiko.awaji.jpg
      text: >+
        こちらのつり橋は、明石海峡大橋です。左側は淡路島です。淡路サービスエリアの観覧車が見えてますね。神戸から淡路島に行くときは車で渡ります。でも鉄道がないのが残念です。

        こちらの浜は、垂水区の舞子浜です。浜の北側高台には、外国人の方々が大勢住んでおられます。

        やはり外国の方は眺めの良い場所が大好きですね。お目が高いですね。。。

  heading: What we offer
  description: >
    Kaldi is the ultimate spot for coffee lovers who want to learn about their
    java’s origin and support the farmers that grew it. We take coffee
    production, roasting and brewing seriously and we’re glad to pass that
    knowledge to anyone. This is an edit via identity...
main:
  heading: 購入物件＊賃貸物件の周辺環境。
  description: |+
    町並みは気に入っている。でも環境はどうか？
    一番心配ですね。
    学校区の評判また通える私立学校をお客様にお伝えできる。
    特に兵庫県は有名な灘中をはじめとする私立の名門進学校が多くあります。
    お子様がおられる家族様にはとても気になりますね。
    そのような質問にもお答えできる地域密着型の弊社に安心してご相談ください。


  image1:
    alt: 新神戸の布引ハーブ園で撮影したお花。色がとても綺麗でしょ。。。
    image: /img/ha-buenhana.jpg
  image2:
    alt: ハーブ園に行く途中にある布引の滝。風雨で迫力ありますね。普段は静かに流れています。
    image: /img/nunobikitaki.jpg
  image3:
    alt: タワーサイドホテル
    image: /img/神戸虹.jpg
testimonials:
  - quote: 美しい眺めの神戸
    author: 素晴らしい環境の神戸
full_image: /img/koubeyakei.jpg
pricing:
  heading: 売り物件のご紹介！
  description: 売り物件2軒。月極ガレージ1台。のご紹介です。
  plans:
    - description: 最寄り駅阪急王子公園徒歩5分
      items:
        - 大手ハウスメーカーセキスイハウスの戸建
        - 平成8年4月築"
        - 土地面積37.63㎡延べ床面積54.64㎡"
      plan: 灘区福住通5丁目戸建て
      price: 1,680万円
    - description: 最寄り駅JR山陽本線加古川駅1800ｍ
      items:
        - 大手ハウスメーカーセキスイハイムの戸建
        - 昭和60年8月築
        - 土地面積137.99㎡延べ床面積94.21㎡
      plan: 加古川市野口町坂元戸建
      price: 1,200万円
    - description: 阪急王子公園駅徒歩1分
      items:
        - 駅近、水道筋商店街すぐ！
      plan: 貸ガレージ
      price: 22,000円（税込）
---
