---
templateKey: blog-post
title: 売物件のご紹介　神戸市灘区　阪急王子公園駅徒歩５分
date: 2021-01-30T02:56:31.265Z
description: |-
  神戸市灘区福住通5丁目　
  水道筋商店街へは南に徒歩3分。
  摩耶小学校へは北に徒歩2分。

  map　⇒　https://goo.gl/maps/NhJ5coXMpqKU5NfEA
featuredpost: true
featuredimage: /img/hukuzumishoumenn.jpg
tags:
  - 福住戸建売り物件
---
![](/img/hukuzumishoumenn.jpg)

## セキスイハウスの戸建　築25年　摩耶小学校、上野中学校人気の校区　価格￥１６８０万